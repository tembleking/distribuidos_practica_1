MIX_ENV=prod
ADDR=127.0.0.1
MASTER_ADDR=$(ADDR)
FIB_MASTER_PROC=main
FIB_MASTER_ADDR=master@$(MASTER_ADDR)
COOKIE=mysecret
MIN_PORT=32000
MAX_PORT=32009

export ELIXIR_ERL_OPTIONS="-kernel inet_dist_listen_min $(MIN_PORT) inet_dist_listen_max $(MAX_PORT)"

.PHONY: epmd

fib: epmd
	MIX_ENV=$(MIX_ENV) mix escript.build

epmd:
	epmd -daemon

master: fib
	./fib \
		--master \
		--master-proc $(FIB_MASTER_PROC) \
		--name $(FIB_MASTER_ADDR) \
		--cookie $(COOKIE)

pc: fib
	./fib \
		--pc \
		--master-addr $(FIB_MASTER_ADDR) \
		--master-proc $(FIB_MASTER_PROC) \
		--name pc-$$RANDOM@$(ADDR) \
		--cookie $(COOKIE)

worker: fib
	./fib \
		--worker \
		--master-addr $(FIB_MASTER_ADDR) \
		--master-proc $(FIB_MASTER_PROC) \
		--name worker-$$RANDOM@$(ADDR) \
		--cookie $(COOKIE)

client: fib
	./fib \
		--client \
		--client-mode $(MODE) \
		--master-addr $(FIB_MASTER_ADDR) \
		--master-proc $(FIB_MASTER_PROC) \
		--name client-$$RANDOM@$(ADDR) \
		--cookie $(COOKIE)


clean:
	-epmd -kill
	rm fib
	rm -rf _build
