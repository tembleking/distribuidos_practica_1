# Practica Distribuidos 1

## Requisitos

Para la ejecución de este código es necesario tener instalado en la máquina a ejecutarlo Elixir >= 1.5.

## Arquitectura

El código compila a un único ejecutable que es capaz de funcionar en 4 modos de operación, dependiendo de los parámetros con los que se le invoque:

- Master
- PoolController
- Worker
- Client

## Compilación

Para compilar el proyecto es necesario ejecutar en la carpeta raiz del mismo la instrucción `make` o `make fib`. 
Esto generará un ejecutable `fib` en la misma carpeta raiz.

## Ejecución 

Por simplicidad se va a utilizar la instrucción `make` para lanzar los distintos modos de ejecución.
Todos los argumentos de la intrucción `make` aquí mostrados son modificables y están descritos en la siguiente tabla:

| Argumento       | Defecto          | Descripción                                                                                                                                                  |
|-----------------|------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| MIX_ENV         | `prod`           | Modo de compilación del proyecto. `dev` incluye mensajes de debugging. `prod` compila en modo Producción. Para más info ver la documentación de Elixir Mix.  |
| ADDR            | 127.0.0.1        | Dirección de escucha para que otros procesos puedan contactar con el proceso a ejecutar.                                                                     |
| MASTER_ADDR     | 127.0.0.1        | Dirección del Master para poder contactar con él.                                                                                                            |
| FIB_MASTER_PROC | `main`           | Nombre de proceso del master para poder contactar con él.                                                                                                    |
| FIB_MASTER_ADDR | master@127.0.0.1 | Dirección (nombre e IP) del master. Si se especifica ADDR se tomará `master@$(MASTER_ADDR)` como nombre e IP.                                                |
| COOKIE          | `mysecret`       | String con la cookie para que todos los procesos puedan hablar entre sí. Todos deben ejecutarse con la misma cookie, o no podrán conectar.                   |
| MIN_PORT        | 32000            | Puerto mínimo de escucha para Elixir. Restringido por el laboratorio de la EINA.                                                                             |
| MAX_PORT        | 32009            | Puerto máximo de escucha para Elixir. Restringido por el laboratorio de la EINA.                                                                             |
| MODE            |                  | Modo de ejecución para un cliente. Se debe especificar si se lanza en modo `client`. Valores posibles: `uno`, `dos`, `tres`.                                 |


### Ejemplos

Invocación a Master en una máquina con IP: 155.250.149.191

```sh
$ make master ADDR=155.250.194.191
```

Invocación a PC en una máquina con IP: 155.250.149.192

```sh
$ make pc ADDR=155.250.194.192 MASTER_ADDR=155.250.194.191
```

Invocación a un Worker en una máquina con IP: 155.250.194.193

```sh
$ make worker ADDR=155.250.194.193 MASTER_ADDR=155.250.194.191
```

Invocación a un Client en una máquina con IP: 155.250.194.194 con Modo "Uno"

```sh
$ make client ADDR=155.250.194.194 MASTER_ADDR=155.250.194.191 MODE=uno
```

