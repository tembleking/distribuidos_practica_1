#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: cliente.ex
#TIEMPO: 10 min
#DESCRIPCION: Contiene el funcionamiento basico del cliente en 3 modos de ejecucion.

defmodule Cliente do
  @moduledoc false

  def launch(pid, op, 1, profile) do
    send(pid, {self(), op, 1..36, 1})
    receive do
      {:result, l} -> IO.inspect l
    end
    {profiler, id} = profile
    send(profiler, {:stop, id, Time.utc_now()})
  end

  def launch(pid, op, n, profiler) when n != 1 do
    # create thread for each sent message
    spawn_link fn ->
      send(pid, {self(), op, 1..36, n})
      receive do
        {:result, l} ->
          IO.inspect l
      end
    end
    launch(pid, op, n - 1, profiler)
  end

  def genera_workload(server_pid, escenario, time, profile) do
    {profiler, id} = profile
    send(profiler, {:start, id, Time.utc_now()})
    cond do
      time <= 3 -> launch(server_pid, :fib, 8, profile); Process.sleep(2000)
      time == 4 -> launch(server_pid, :fib, 8, profile); Process.sleep(round(:rand.uniform(100) / 100 * 2000))
      time <= 8 -> launch(server_pid, :fib, 8, profile); Process.sleep(round(:rand.uniform(100) / 1000 * 2000))
      time == 9 -> launch(server_pid, :fib_tr, 8, profile); Process.sleep(round(:rand.uniform(2) / 2 * 2000))
    end
    profile = {profiler, id + 1}
    genera_workload(server_pid, escenario, rem(time + 1, 10), profile)
  end

  def genera_workload(server_pid, escenario, profile) do
    {profiler, id} = profile
    send(profiler, {:start, id, Time.utc_now()})
    if escenario == 1 do
      launch(server_pid, :fib, 1, profile)
    else
      launch(server_pid, :fib, 4, profile)
    end
    Process.sleep(2000)
    profile = {profiler, id + 1}
    genera_workload(server_pid, escenario, profile)
  end


  def cliente(server_pid, tipo_escenario) do
    profile = Profiler.new()
    case tipo_escenario do
      :uno -> genera_workload(server_pid, 1, profile)
      :dos -> genera_workload(server_pid, 2, profile)
      :tres -> genera_workload(server_pid, 3, 1, profile)
    end
  end
end