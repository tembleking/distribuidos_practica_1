#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: client.ex
#TIEMPO: 5 min
#DESCRIPCION: Contiene el punto de entrada a la ejecucion del cliente.
#             Se ha querido separar esta implementacion del funcionamiento definido en cliente.ex

defmodule Client do
  @moduledoc false

  def new(args) do
    master = Connection.connect(args)
    Cliente.cliente(master, String.to_atom(args[:client_mode]))
  end
end
