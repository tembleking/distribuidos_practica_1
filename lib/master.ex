#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: master.ex
#TIEMPO: 1h 30 min
#DESCRIPCION: Especifica el comportamiento del Master.
#             Cuando no existe un PC asignado, espera a que se levante uno antes de
#             escuchar peticiones de trabajo o Workers encolados.
#             Cuando un PC indica que esta disponible, comienza a enviarle peticiones de registro de Workers
#             y cargas de trabajo de Clients.

defmodule Master do
  def new(args) do
    Connection.listen(args)
    master()
  end

  defp master(pool_controller_pid \\ nil) do
    {pool_controller_pid} = receive do

      {client, op, list, n} when pool_controller_pid != nil ->
        IO.puts("Client requires some work...")
        send(pool_controller_pid, {self(), :requestWorker, client, op, list, n})
        {pool_controller_pid}

      {:registerWorker, new_worker} when pool_controller_pid != nil ->
        IO.puts("Worker registered: #{inspect(new_worker)}")
        IO.puts("Current connected nodes: #{inspect(Node.list)}")
        send(pool_controller_pid, {:registerWorker, new_worker})
        {pool_controller_pid}

      {:registerPoolController, new_pool_ctrl_pid} when pool_controller_pid == nil ->
        IO.puts("New pool controller: #{inspect(new_pool_ctrl_pid)}")
        IO.puts("Current connected nodes: #{inspect(Node.list)}")
        {new_pool_ctrl_pid}

    end

    master(pool_controller_pid)
  end
end
