#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: cliente.ex
#TIEMPO: 10 min
#DESCRIPCION: Contiene el funcionamiento basico del worker. Se mantendra en ejecucion hasta
#             que se le ordene parar (Actualmente no se le ordena parar, la parada es manual)
#             No tiene mayor logica que la de esperar a morir.
#             El trabajo es enviado directamente por el pool_controller via Node.spawn()

defmodule Worker do
  @moduledoc false

  def new(args) do
    master = Connection.connect(args)
    send(master, {:registerWorker, Node.self()})
    work()
  end

  defp work() do
    _myself = Node.self()
    receive do
      {:turnOffWorker, _myself} -> IO.puts("Closing worker...")
                                  System.halt(0)
    end
  end
end
