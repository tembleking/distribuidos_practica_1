#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: pool_controller.ex
#TIEMPO: 1h 30 min
#DESCRIPCION: Define el comportamiento del PoolController. Se autoregistra como PC en el Master y recibe de este las
#             peticiones de trabajo, que envia a ejecutar en los worker.
#             El PC no habla directamente con los Client, sino que indica a los Worker la direccion del Client
#             para responder a su carga de trabajo.
#             El PC no escuchara mensajes de carga de trabajo hasta que no tenga asignado al menos un Worker.

defmodule PoolController do
  use GenServer
  @moduledoc false

  def init(_) do
    Process.flag(:trap_exit, true)
    {:ok, {}}
  end

  def new(args) do
    master_pid = Connection.connect(args)
    send(master_pid, {:registerPoolController, self()})
    pool_controller()
  end

  defp pool_controller(worker_list \\ []) do
    new_node_list = receive do
      {_master, :requestWorker, client, :fib, list, n} when worker_list != [] ->
        IO.puts("Requesting some work from client #{inspect(client)}")
        IO.puts("Workers: #{inspect(worker_list)}")
        [worker | new_worker_list] = worker_list
        parent = self()
        Node.spawn(
          worker,
          fn ->
            result = Enum.map(list, fn x -> Fib.fibonacci(x) end)
            send(client, {:result, result})
            send(parent, {:finishedWorking, n, Node.self()})
          end
        )
        new_worker_list

      {_master, :requestWorker, client, :fib_tr, list, n} when worker_list != [] ->
        IO.puts("Requesting some work from client #{inspect(client)} (tr)")
        IO.puts("Workers: #{inspect(worker_list)}")
        [worker | new_worker_list] = worker_list
        parent = self()
        Node.spawn(
          worker,
          fn ->
            result = Enum.map(list, fn x -> Fib.fibonacci_tr(x) end)
            send(client, {:result, result})
            send(parent, {:finishedWorking, n, Node.self()})
          end
        )
        new_worker_list

      {:registerWorker, new_worker} ->
        IO.puts("New worker registered: #{inspect(new_worker)}")
        worker_list ++ [new_worker]

      {:finishedWorking, id, worker} ->
        IO.puts("Worker #{inspect(worker)} finished doing task number #{id}")
        worker_list ++ [worker]
    end
    pool_controller(new_node_list)
  end
end