#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: profiler.ex
#TIEMPO: 30 min
#DESCRIPCION: Contiene el funcionamiento basico del profiler.
#             Se encarga de registrar de forma ordenada los tiempos de ejecucion
#             que le marquen por cada ID de ejecucion

defmodule Profiler do
  @moduledoc false

  def new(), do: {(spawn_link fn -> profile() end), 0}

  defp profile(last_report \\ nil, id \\ 0) do
    {last_report, id} = receive do
      {:start, recv_id, time} when recv_id == id -> {time, id}
      {:stop, recv_id, time} when last_report != nil and recv_id == id ->
        time_diff = Time.diff(time, last_report, :millisecond)
        IO.puts(:stderr, ">>> Task ##{id} lasted: #{time_diff} milliseconds")
        {nil, id + 1}
    end
    profile(last_report, id)
  end
end
