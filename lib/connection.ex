#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: connection.ex
#TIEMPO: 30 min
#DESCRIPCION: Define como se va a realizar la conexion entre todos los Nodos.
#             Listen es ejecutado por el Master
#             connect es ejecutado por los demas roles.


defmodule Connection do
  @moduledoc false

  def listen(args) do
    Process.register(self(), String.to_atom(args[:master_proc]))
    enable_distributed(args)
  end

  defp enable_distributed(args) do
    name = String.to_atom(args[:name])

    cookie = String.to_atom(args[:cookie])


    if !Node.alive? do
      IO.puts(":: Setting node name as #{args[:name]}...")
      IO.puts(":: Loading distributed mode...")
      Node.start(name)
    end

    IO.puts(":: Setting cookie [not shown]...")
    Node.set_cookie(cookie)
  end

  def connect(args) do
    master_addr = String.to_atom(args[:master_addr])
    master = {String.to_atom(args[:master_proc]), master_addr}

    enable_distributed(args)

    IO.puts(":: Connecting to master #{args[:master_addr]}...")
    case Node.connect(master_addr) do
      n when n in [:ignored, false] ->
        IO.puts ":: Error: Cannot connect to master"
        System.halt(1)
      true -> true
    end

    IO.puts(":: Connected!")

    master
  end
end
