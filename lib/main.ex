#AUTORES: Federico Barcelona Auria y Diego Caballe Casanova
#NIAs: 666151 y 738712
#FECHA: 11/Oct/2019
#FICHERO: main.ex
#TIEMPO: 1h
#DESCRIPCION: Define el rol a ejecutar, dependiendo del parametro especificado:
#             --master    Se comportara como Master
#             --pc        Se comportara como PoolController
#             --worker    Se comportara como Worker
#             --client    Se comportara como Client

defmodule Main do
  @moduledoc false

  def main(args \\ []) do
    args
    |> parse_args
    |> exec
  end

  defp parse_args(args) do
    {parsed, argv, _errors} =
      args
      |> OptionParser.parse(
           strict: [
             name: :string,
             cookie: :string,
             master: :boolean,
             worker: :boolean,
             pc: :boolean,
             master_addr: :string,
             master_proc: :string,
             client_mode: :string,
             client: :boolean,
             help: :boolean,
           ]
         )

    {parsed, argv}
  end

  defp exec({args, _argv}) do

    cond do
      args[:help] -> help()
      args[:cookie] == nil -> IO.puts("No cookie defined. Define one with --cookie")
      args[:name] == nil -> IO.puts("No cookie defined. Define one with --name")
      args[:master_proc] == nil -> IO.puts("You need to specify a --master-proc")
      args[:master] -> Master.new(args)
      args[:master_addr] == nil -> IO.puts("You need to specify a --master-addr")
      args[:worker] -> Worker.new(args)
      args[:pc] -> PoolController.new(args)
      args[:client_mode] == nil -> IO.puts("You need to specify a --client-mode")
      args[:client] -> Client.new(args)
      true -> help()
    end
  end

  defp help() do
    IO.puts("./fib --cookie <COOKIE> <ARGS>")
    IO.puts("")
    IO.puts(" ARGS:")
    IO.puts("   --master            Launches the program as Master")
    IO.puts("   --pc                Launches the program as Pool Controller")
    IO.puts("   --worker            Launches the program as Worker")
    IO.puts("   --client            Launches the program as Client")
    IO.puts("   --client-mode       Defines the client mode: 'uno', 'dos', or 'tres'")
    IO.puts("   --master-addr       Defines the address of the Master")
    IO.puts("   --master-proc       Defines the process atom of the Master")
    IO.puts("   --name              Defines how this node will be recognized as (e.g.: node1@127.0.0.1)")
    IO.puts("   --cookie            Defines the connection cookie of the cluster")
    IO.puts("   -h, --help          Shows help")
  end

end
